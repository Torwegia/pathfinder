#ifndef PATH_RAY_C
#define PATH_RAY_C

#include "../common_core.h"
#include "../core_geometry.h"

class Ray
{
    public:
        Ray();
        Ray(const Point& p, const Vector& v, float start, 
            float end = INFINITY, float t = 0.0f, int d = 0);//fresh rays
        Ray(const Point& p, const Vector& v, const Ray& par,
            float start, float end = INFINITY);//bounces

        Point operator()(const float& t) const;
        
        //geometric definition
        Point o;
        Vector d;

        //useful for renderering
        mutable float tMin, tMax;
        float time;
        int depth;
};

#endif
