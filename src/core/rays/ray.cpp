#include "ray.h"

Ray::Ray()
:tMin(0.0f), tMax(INFINITY), time(0.0f), depth(0)
{}

Ray::Ray(const Point& p, const Vector& v, float start, 
         float end, float t, int d)
:o(p), d(v), tMin(start), tMax(end), time(t), depth(d)
{
    this->d.normalize();
}

Ray::Ray(const Point& p, const Vector& v, const Ray& par, 
         float start, float end)
:o(p), d(v), tMin(start), tMax(end), time(par.time), depth(par.depth+1)
{}

Point Ray::operator()(const float& t) const
{
    return o + d*t;
}
