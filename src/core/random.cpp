#include "random.h"

std::random_device Random::rd;
std::mt19937 Random::gen(rd());

float Random::UniformFloat(float a, float b)
{
    std::uniform_real_distribution<> dist(a, b);
    return dist(gen);
}

float Random::NormalFloat(float mean, float stddev)
{
    std::normal_distribution<> dist(mean, stddev);
    return dist(gen);
}

Vector Random::UniformVec(float a, float b)
{
    std::uniform_real_distribution<> dist(a, b);
    return Vector( dist(gen), dist(gen), dist(gen) );
}

Vector Random::NormalVec(float mean, float stddev)
{
    std::normal_distribution<> dist(mean, stddev);
    return Vector( dist(gen), dist(gen), dist(gen) );
}
