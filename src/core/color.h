#ifndef PATH_COLOR_H
#define PATH_COLOR_H

class Color
{
    public:
        Color();
        Color(float, float, float);

        Color operator*(float s) const;
        Color& operator*=(float s);
        Color operator/(float s) const;
        Color& operator/=(float s);

        Color operator*(Color a) const;
        Color& operator*=(Color a);
        Color operator/(Color a) const;
        Color& operator/=(Color a);

        Color operator+(Color a) const;
        Color& operator+=(Color a);
        Color operator-(Color a) const;
        Color& operator-=(Color a);

        void Clamp();

        float r,g,b;
};

#endif
