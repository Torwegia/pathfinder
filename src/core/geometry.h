#ifndef PATH_GEO_C
#define PATH_GEO_C

#include "common_core.h"

#include "math/point.h"
#include "math/vector.h"
#include "math/normal.h"
#include "math/ray.h"

//random helper functions

inline Vector operator*(const float s, const Vector& v)
{
    return v*s;
}

inline float dot(const Vector& a, const Vector& b)
{
    return (a.x*b.x + a.y*b.y + a.z*b.z);
}
inline float dot(const Vector& a, const Normal& b)
{
    return (a.x*b.x + a.y*b.y + a.z*b.z);
}
inline float dot(const Normal& a, const Vector& b)
{
    return (a.x*b.x + a.y*b.y + a.z*b.z);
}
inline float dot(const Normal& a, const Normal& b)
{
    return (a.x*b.x + a.y*b.y + a.z*b.z);
}

inline Vector cross(const Vector& a, const Vector& b)
{
    return Vector((a.y*b.z) - (a.z*b.y),
            (a.z*b.x) - (a.x*b.z),
            (a.x*b.y) - (a.y*b.x));
}

inline void localCoord(const Vector& a, Vector& b, Vector& c)
{
    float invLength = 1 / sqrt(a.x*a.x + a.y*a.y);
    b = Vector(-a.y * invLength, a.x * invLength, 0);
    c = cross(a,b);
}


#endif
