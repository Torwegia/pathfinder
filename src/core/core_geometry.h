#ifndef PATH_GEO_C
#define PATH_GEO_C

#include "common_core.h"

#include "geometry/point.h"
#include "geometry/vector.h"
#include "geometry/normal.h"
#include "rays/ray.h"

//random helper functions

inline Vector operator*(const float s, const Vector& v)
{
    return v*s;
}

inline Point operator*(const float s, const Point& p)
{
    return p*s;
}

inline float dot(const Vector& a, const Vector& b)
{
    return (a.x*b.x + a.y*b.y + a.z*b.z);
}
inline float dot(const Vector& a, const Normal& b)
{
    return (a.x*b.x + a.y*b.y + a.z*b.z);
}
inline float dot(const Normal& a, const Vector& b)
{
    return (a.x*b.x + a.y*b.y + a.z*b.z);
}
inline float dot(const Normal& a, const Normal& b)
{
    return (a.x*b.x + a.y*b.y + a.z*b.z);
}

inline Vector cross(const Vector& a, const Vector& b)
{
    return Vector((a.y*b.z) - (a.z*b.y),
            (a.z*b.x) - (a.x*b.z),
            (a.x*b.y) - (a.y*b.x));
}

inline void localCoord(const Vector& a, Vector& b, Vector& c)
{
    if(fabs(a.x) > fabs(a.y))
    {
        float invLength = 1 / sqrt(a.x*a.x + a.z*a.z);
        b = Vector(-a.z * invLength, 0.0,  a.x * invLength);
    }
    else
    {
        float invLength = 1 / sqrt(a.y*a.y + a.z*a.z);
        b = Vector(0.0, a.z * invLength, -a.y * invLength);
    }
    c = cross(a,b);
}


#endif
