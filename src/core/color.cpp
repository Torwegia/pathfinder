#include "color.h"
#include <algorithm>

Color::Color()
:r(0.0), g(0.0), b(0.0)
{}

Color::Color(float x, float y, float z)
:r(x), g(y), b(z)
{}

Color Color::operator*(float s) const
{
    return Color(r*s, g*s, b*s);
}

Color& Color::operator*=(float s)
{
    r *= s;
    g *= s;
    b *= s;
    return *this;
}

Color Color::operator/(float s) const
{
    return Color(r/s, g/s, b/s);
}

Color& Color::operator/=(float s)
{
    r /= s;
    g /= s;
    b /= s;
    return *this;
}

Color Color::operator*(Color a) const
{
    return Color(r*a.r, g*a.g, b*a.b);
}

Color& Color::operator*=(Color a)
{
    r *= a.r;
    g *= a.g;
    b *= a.b;
    return *this;
}

Color Color::operator/(Color a) const
{
    return Color(r/a.r, g/a.g, b/a.b);
}

Color& Color::operator/=(Color a)
{
    r /= a.r;
    g /= a.g;
    b /= a.b;
    return *this;
}

Color Color::operator+(Color a) const
{
    return Color(r+a.r, g+a.g, b+a.b);
}

Color& Color::operator+=(Color a)
{
    r += a.r;
    g += a.g;
    b += a.b;
    return *this;
}

Color Color::operator-(Color a) const
{
    return Color(r-a.r, g-a.g, b-a.b);
}

Color& Color::operator-=(Color a)
{
    r -= a.r;
    g -= a.g;
    b -= a.b;
    return *this;
}

void Color::Clamp()
{
    r = std::min(1.0f, std::max(0.0f, r));
    g = std::min(1.0f, std::max(0.0f, g));
    b = std::min(1.0f, std::max(0.0f, b));
}
