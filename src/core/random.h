#ifndef PATH_RANDOM_H
#define PATH_RANDOM_H

#include "core_geometry.h"

#include <random>

class Random
{
    public:
        static float UniformFloat(float a, float b);
        static float NormalFloat(float mean, float stddev);

        static Vector UniformVec(float a, float b);
        static Vector NormalVec(float mean, float stddev);
    private:
        Random();

        static std::random_device rd;
        static std::mt19937 gen;
};
        

#endif
