#include "normal.h"

Normal::Normal()
{
    x = y = z = 0.0f;
}

Normal::Normal(const Normal& n)
:x(n.x), y(n.y), z(n.z)
{
    if ( isnan(x) || isnan(y) || isnan(z) )
    {
        std::cerr << "[F]   Bad Normal Constructed" << std::endl;
        exit(-1);
    }
}

Normal::Normal(const float& a, const float& b, const float& c)
:x(a), y(b), z(c)
{
    if ( isnan(x) || isnan(y) || isnan(z) )
    {
        std::cerr << "[F]   Bad Normal Constructed" << std::endl;
        exit(-1);
    }
}

Normal::Normal(const Vector& v)
:x(v.x), y(v.y), z(v.z)
{
    if ( isnan(x) || isnan(y) || isnan(z) )
    {
        std::cerr << "[F]   Bad Normal Constructed" << std::endl;
        exit(-1);
    }
}

Normal Normal::operator* (const float& s) const
{
    return Normal(x*s, y*s, z*s);
}

Normal& Normal::operator*=(const float& s)
{
    x *= s;
    y *= s;
    z *= s;
    return *this;
}

Normal Normal::operator/(const float &s) const
{
    return Normal(x/s, y/s, z/s);
}

Normal& Normal::operator/=(const float& s)
{
    x /= s;
    y /= s;
    z /= s;
    return *this;
}

Normal Normal::operator+(const Normal &r) const
{
    return Normal(x+r.x, y+r.y, z+r.z);
}

Normal& Normal::operator+=(const Normal &r)
{
    x += r.x;
    y += r.y;
    z += r.z;
    return *this;
}

Normal Normal::operator-(const Normal& r) const
{
    return Normal(x-r.x, y-r.y, z-r.z);
}

Normal& Normal::operator-=(const Normal& r)
{
    x -= r.x;
    y -= r.y;
    z -= r.z;
    return *this;
}

Normal Normal::operator-() const
{
    return Normal(-x, -y, -z);
}

void Normal::normalize()
{
    float invLength = 1 / sqrt(x*x + y*y + z*z);
    x *= invLength;
    y *= invLength;
    z *= invLength;
}
