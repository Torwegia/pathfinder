#ifndef PATH_BBOX_C
#define PATH_BBOX_C

#include "../common_core.h"
#include "../core_geometry.h"
#include "point.h"

class BBox
{
    public:
        BBox();
        BBox(const BBox& r);
        BBox(const Point& p);
        BBox(const Point& p0, const Point& p1);
        
        //sphere to bias ray creation later
        void BSphere(Point& p, float& rad) const;

        //create a BBox that includes both arguments
        BBox Union(const BBox& b);
        BBox Union(const Point& p);
        static BBox Union(const BBox& b1, const BBox& b2);
        static BBox Union(const BBox& b, const Point& p);
        
        //random tests
        bool overlaps(const BBox& b) const;
        static bool overlaps(const BBox& b1, const BBox& b2);
        bool inside(const Point& p) const;
        static bool inside(const BBox& b, const Point& p);

        //size related things
        void expand(float scale);
        float sa() const;
        float vol() const;
        
        //useful for the kd-tree
        int maxAxis() const;//0-x 1-y 2-z

        Point pMin, pMax;
};


#endif
