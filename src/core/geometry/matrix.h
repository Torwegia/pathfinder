#ifndef PATH_MATRIX_C
#define PATH_MATRIX_C

#include "../common_core.h"

//mainly used for transforming things into affine spaces
//there fore possibly not complete
class Matrix4
{
    public:
        Matrix4();//identity
        Matrix4(const Matrix4& mat);
        Matrix4(const float mat[4][4]);
        Matrix4(float i11, float i12, float i13, float i14,
                float i21, float i22, float i23, float i24,
                float i31, float i32, float i33, float i34,
                float i41, float i42, float i43, float i44);

        bool operator==(const Matrix4& mat);
        bool operator!=(const Matrix4& mat);

        Matrix4 operator*(const Matrix4& mat) const;

        Matrix4 transpose();
        Matrix4 inverse();

        float m[4][4];
};


#endif
