#ifndef PATH_POINT_C
#define PATH_POINT_C

#include "../common_core.h"
#include "vector.h"

class Point
{
    public:
        Point();
        Point(const Point& p);
        Point(const float& a, const float& b, const float& c);

        Point& operator=(const Point& r);

        Point operator+(const Vector& v) const;
        Point& operator+=(const Vector& v);
        Point operator-(const Vector& v) const;
        Point& operator-=(const Vector& v);

        //nonsensical operators
        Point operator*(const float) const;
        Point operator+(const Point&) const;

        Vector operator-(const Point& p) const;

        float x, y, z;
};

#endif
