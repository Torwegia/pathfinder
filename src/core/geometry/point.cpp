#include "point.h"

Point::Point()
:x(0.0f), y(0.0f), z(0.0)
{

}

Point::Point(const Point& p)
:x(p.x), y(p.y), z(p.z)
{

}

Point::Point(const float& a, const float& b, const float& c)
:x(a), y(b), z(c)
{
    if ( isnan(x) || isnan(y) || isnan(z) )
    {
        std::cerr << "[F]   Point constructed" << std::endl;
        exit(-1);
    }
}

Point& Point::operator=(const Point& r)
{
    if( this != &r)
    {
        x = r.x;
        y = r.y;
        z = r.z;
    }
    return *this;
}

Point Point::operator+(const Vector &v) const
{
    return Point(x+v.x, y+v.y, z+v.z);
}

Point& Point::operator+=(const Vector &v)
{
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}

Point Point::operator-(const Vector &v) const
{
    return Point(x-v.x, y-v.y, z-v.z);
}

Point& Point::operator-=(const Vector &v)
{
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}

Point Point::operator*(const float s) const
{
    return Point(x*s, y*s, z*s);
}

Point Point::operator+(const Point& p) const
{
    return Point(x+p.x, y+p.y, z+p.z);
}

Vector Point::operator-(const Point &p) const
{
    return Vector(x-p.x, y-p.y, z-p.z);
}
