#ifndef PATH_VECTOR_H
#define PATH_VECTOR_H

class Vector;

#include "../common_core.h"
#include "normal.h"

class Vector
{
    public:
        Vector();
        Vector(const Vector& r);
        Vector(const float& a, const float& b, const float& c);

        Vector& operator=(const Vector& r);

        explicit Vector(const Normal &n);

        Vector operator*(const float& s) const;
        Vector& operator*=(const float& s);
        Vector operator/(const float& s) const;
        Vector& operator/=(const float& s);

        Vector operator+(const Vector& r) const;
        Vector& operator+=(const Vector& r);
        Vector operator-(const Vector& r) const;
        Vector& operator-=(const Vector& r);

        void normalize();
        Vector norm();

        float length() const;
        float lenSq() const;

        Vector operator-() const;

        float x,y,z;
};
#endif
