#include "bbox.h"
using std::max;
using std::min;

BBox::BBox()
{
    pMax = Point(-INFINITY, -INFINITY, -INFINITY);
    pMin = Point(INFINITY, INFINITY, INFINITY);
}

BBox::BBox(const BBox &r)
:pMin(r.pMin), pMax(r.pMax)
{}

BBox::BBox(const Point &p)
:pMin(p), pMax(p)
{}

BBox::BBox(const Point &p1, const Point &p2)
{
    pMin = Point(min(p1.x,p2.x), min(p1.y, p2.y), min(p1.z, p2.z));
    pMax = Point(max(p1.x,p2.x), max(p1.y, p2.y), max(p1.z, p2.z));
}

void BBox::BSphere(Point &p, float &rad) const
{
    //put the values for a sphere containing this bounding box.
    //set p equal to a point midway between min and max
    p = 0.5f * pMin + 0.5f * pMax;
    rad = inside(p) ? ( (p-pMin).length()) : 0.0f;
}

BBox BBox::Union(const BBox &b)
{
    //combine the two
    BBox ret;
    ret.pMin.x = min(pMin.x , b.pMin.x);
    ret.pMin.y = min(pMin.y , b.pMin.y);
    ret.pMin.z = min(pMin.z , b.pMin.z);
    ret.pMax.x = max(pMax.x , b.pMax.x);
    ret.pMax.y = max(pMax.y , b.pMax.y);
    ret.pMax.z = max(pMax.z , b.pMax.z);
    return ret;
}

BBox BBox::Union(const Point &p)
{
    //include the point
    BBox ret;
    ret.pMin.x = min(pMin.x , p.x);
    ret.pMin.y = min(pMin.y , p.y);
    ret.pMin.z = min(pMin.z , p.z);
    ret.pMax.x = max(pMax.x , p.x);
    ret.pMax.y = max(pMax.y , p.y);
    ret.pMax.z = max(pMax.z , p.z);
    return ret;
}

BBox BBox::Union(const BBox &b1, const BBox &b2)
{
    BBox ret;
    ret.pMin.x = min(b1.pMin.x , b2.pMin.x);
    ret.pMin.y = min(b1.pMin.y , b2.pMin.y);
    ret.pMin.z = min(b1.pMin.z , b2.pMin.z);
    ret.pMax.x = max(b1.pMax.x , b2.pMax.x);
    ret.pMax.y = max(b1.pMax.y , b2.pMax.y);
    ret.pMax.z = max(b1.pMax.z , b2.pMax.z);
    return ret;
}

BBox BBox::Union(const BBox &b, const Point &p)
{
    BBox ret;
    ret.pMin.x = min(b.pMin.x , p.x);
    ret.pMin.y = min(b.pMin.y , p.y);
    ret.pMin.z = min(b.pMin.z , p.z);
    ret.pMax.x = max(b.pMax.x , p.x);
    ret.pMax.y = max(b.pMax.y , p.y);
    ret.pMax.z = max(b.pMax.z , p.z);
    return ret;
}

bool BBox::overlaps(const BBox &b) const
{
    bool r1,r2,r3;
    r1 = (pMax.x >= b.pMin.x) && (pMin.x <= b.pMax.x);
    r2 = (pMax.y >= b.pMin.y) && (pMin.y <= b.pMax.y);
    r3 = (pMax.z >= b.pMin.z) && (pMin.z <= b.pMax.z);
    return r1 && r2 && r3;
}

bool BBox::overlaps(const BBox &b1, const BBox &b2)
{
    bool r1, r2, r3;
    r1 = (b1.pMax.x >= b2.pMin.x) && (b1.pMin.x <= b2.pMax.x);
    r2 = (b1.pMax.y >= b2.pMin.y) && (b1.pMin.y <= b2.pMax.y);
    r3 = (b1.pMax.z >= b2.pMin.z) && (b1.pMin.z <= b2.pMax.z);
    return r1 && r2 && r3;
}

bool BBox::inside(const Point &p) const
{
    bool r1, r2, r3;
    r1 = (p.x >= pMin.x) && (p.x <= pMax.x);
    r2 = (p.y >= pMin.y) && (p.y <= pMax.y);
    r3 = (p.z >= pMin.z) && (p.z <= pMax.z);
    return r1 && r2 && r3;
}

bool BBox::inside(const BBox &b, const Point &p)
{
    bool r1, r2, r3;
    r1 = (p.x >= b.pMin.x) && (p.x <= b.pMax.x);
    r2 = (p.y >= b.pMin.y) && (p.y <= b.pMax.y);
    r3 = (p.z >= b.pMin.z) && (p.z <= b.pMax.z);
    return r1 && r2 && r3;
}

void BBox::expand(float scale)
{
    pMin -= Vector(scale, scale, scale);
    pMin += Vector(scale, scale, scale);
}

float BBox::sa() const
{
    Vector r = pMax - pMin;
    return (2.0f * (r.x*r.y + r.x*r.z + r.y*r.z));
}

float BBox::vol() const
{
    Vector r = pMax - pMin;
    return r.x*r.y*r.z;
}

int BBox::maxAxis() const
{
    Vector r = pMax - pMin;
    if(r.x > r.y && r.x > r.z)
        return 0;
    else if(r.y > r.z)
        return 1;
    else
        return 2;
}
