#ifndef PATH_NORMAL_C
#define PATH_NORMAL_C

#include "../common_core.h"
#include "vector.h"

//kinda like a vector...
class Normal
{
    public:
        Normal();
        Normal(const Normal&);
        Normal(const float&, const float&, const float&);

        explicit Normal(const Vector&);

        Normal operator*(const float& s) const;
        Normal& operator*=(const float& s);
        Normal operator/(const float& s) const;
        Normal& operator/=(const float& s);

        Normal operator+(const Normal& r) const;
        Normal& operator+=(const Normal& r);
        Normal operator-(const Normal& r) const;
        Normal& operator-=(const Normal& r);

        void normalize();

        Normal operator-() const;

        float x, y, z;
};

#endif
