#include "vector.h"

Vector::Vector()
{
    x = y = z = 0.0f;
}

Vector::Vector(const Vector& v)
:x(v.x), y(v.y), z(v.z)
{

}

Vector::Vector(const float& a, const float& b, const float& c)
:x(a), y(b), z(c)
{
    if ( isnan(x) || isnan(y) || isnan(z) )
    {
        std::cerr << "[F]   Invalid Vector constructed" << std::endl;
        exit(-1);
    }
}

Vector::Vector(const Normal& n)
:x(n.x), y(n.y), z(n.z)
{

}

Vector Vector::operator* (const float& s) const
{
    return Vector(x*s, y*s, z*s);
}

Vector& Vector::operator*=(const float& s)
{
    x *= s;
    y *= s;
    z *= s;
    return *this;
}

Vector Vector::operator/(const float &s) const
{
    return Vector(x/s, y/s, z/s);
}

Vector& Vector::operator/=(const float& s)
{
    x /= s;
    y /= s;
    z /= s;
    return *this;
}

Vector Vector::operator+(const Vector &r) const
{
    return Vector(x+r.x, y+r.y, z+r.z);
}

Vector& Vector::operator+=(const Vector &r)
{
    x += r.x;
    y += r.y;
    z += r.z;
    return *this;
}

Vector Vector::operator-(const Vector& r) const
{
    return Vector(x-r.x, y-r.y, z-r.z);
}

Vector& Vector::operator-=(const Vector& r)
{
    x -= r.x;
    y -= r.y;
    z -= r.z;
    return *this;
}

Vector Vector::operator-() const
{
    return Vector(-x, -y, -z);
}

void Vector::normalize()
{
    float invLength = 1 / sqrt(x*x + y*y + z*z);
    x *= invLength;
    y *= invLength;
    z *= invLength;
}
