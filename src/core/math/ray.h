#ifndef PATH_RAY_C
#define PATH_RAY_C

#include "../common_core.h"
#include "point.h"
#include "vector.h"

class Ray
{
    public:
        Ray();
        Ray(const Point& p, const Vector& v, float start, 
            float end = INIFINTIY, float t = 0.0f, int d = 0);//fresh rays
        Ray(const Point& p, const Vector& v, const Ray& par,
            float start, float end = INIFINITY);//bounces

        Point operator()(const float& t) const;
        
        //geometric definition
        Point o;
        Vector d;

        //useful for renderering
        mutable float tMin, tMax;
        float time;
        int depth;
};

//Ray Differentials are used for AA
//Ray Differentials contain information about two other rays
//They create and area on a surface which can be averaged to provide proper AA
//Different cameras will be responsible for creating these rays
class RayDiff : public Ray
{
    public:
        RayDiff();
        RayDiff(const Point& p, const Vector& v, float start, 
                float end = INFINITY, float t = 0.0f, int d = 0);
        RayDiff(const Point& p, const vector& v, const Ray& par,
                float start, float end = INIFINITY);

        explicit RayDiff(const Ray& r);

        void ScaleDiffs(const float s);

        bool hasDiff;
        Point rxO, ryO;
        Vector rxD, ryD

}

#endif
