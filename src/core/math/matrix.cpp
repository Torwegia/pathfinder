#include "matrix.h"
Matrix4::Matrix4()
{
    memset(m, 0.0f, 16 * sizeof(float));
    m[0][0] = m[1][1] = m[2][2] = m[3][3] = 1.0f;
}
    
Matrix4::Matrix4(const Matrix4& mat)
{
    memcpy(m, mat.m, 16*sizeof(float));
}

Matrix4::Matrix4(const float mat[4][4])
{
    memcpy(m, mat, 16*sizeof(float));
}

Matrix4(float i00, float i01, float i02, float i03,
        float i10, float i11, float i12, float i13,
        float i20, float i21, float i22, float i23,
        float i30, float i31, float i32, float i33)
{
    m[0][0] = i00; m[0][1] = i01; m[0][2] = i02; m[0][3] = i03;
    m[1][0] = i10; m[1][1] = i11; m[1][2] = i12; m[1][3] = i13;
    m[2][0] = i20; m[2][1] = i21; m[2][2] = i22; m[2][3] = i23;
    m[3][0] = i30; m[3][1] = i31; m[3][2] = i32; m[3][3] = i33;
}

bool Matrix4::operator==(const Matrix4& mat)
{
    for(unsigned int i = 0; i < 4; ++i)
        for(unsigned int j = 0; j < 4; ++j)
            if(m[i][j] != mat.m[i][j])
                return false;

    return true;
}

bool Matrix4::operator!=(const Matrix4& mat)
{
    for(unsigned int i = 0; i < 4; ++i)
        for(unsigned int j = 0; j < 4; ++j)
            if(m[i][j] != mat.m[i][j])
                return true;

    return false;
}

Matrix4 Matrix4::operator*(const Matrix4& mat) const
{
    Matrix4 ret;
    for(unsigned int i = 0; i < 4; ++i)
        for(unsigned int j = 0; j < 4; ++j)
            ret[i][j] = m[i][0] * mat[0][j] +
                        m[i][1] * mat[1][j] +
                        m[i][2] * mat[2][j] +
                        m[i][3] * mat[3][j];
    return ret;
}

Matrix4 Matrix4::transpose()
{
    return Matrix4(m[0][0], m[1][0], m[2][0], m[3][0],
                   m[0][1], m[1][1], m[2][1], m[3][1],
                   m[0][2], m[1][2], m[2][2], m[3][2],
                   m[0][3], m[1][3], m[2][3], m[3][3]);
}

Matrix4 Matrix4::inverse()
{
    //Gauss Jordan Elimination to invert the matrix
    //Thank you numerical recipes in C
    int indxc[4], indxr[4];
    int ipiv[4] = {0, 0, 0, 0};
    float invMat[4][4];
    memcpy(invMat, m, 16 * sizeof(float));

    //columns to be reduced
    for(int j = 0; j < 4; ++j)
    {
        int irow = -1, icol = -1;//detect if something went wron
        float big = 0.0;
        //pivots
        for(int i = 0; i < 4; ++i)
        {
            if(ipiv[i] != 1)
            {
                for(int k =0; k < 4; k++)
                {
                    if(ipiv[k] == 0)
                    {
                        if(fabs(invMat[j][k]) >= big)
                        {
                            big = fabs(invMat[j][k]);
                            irow = j;
                            icol = k;
                        }
                    }
                }
            }
        }
        ++(ipiv[icol]);
        //we now move the pivot elements to the diagonal
        //indxc[i] is the ith pivot, or the ith column to be reduced
        //indxr[i] is the row where is was originally
        if(irow != icol)
        {
            for(int k = 0; k < 4; ++k)
                std::swap(invMat[irow][k], invMat[icol][k]);
        }
        indxc[i] = icol;
        indxr[i] = irow;
        //now we divde the pivot row by the pivot element
        //first make sure there will be no divide by zero
        if(invMat[icol][icol] == 0.0f)
        {
            std::cerr << "Error in Gauss-Jordan inverse, singular matrix" <<
                         std::endl;
            exit(-1);
        }

        //do the divide across the row
        float pivotInv = 1.0/invMat[icol][icol];
        invMat[icol][icol] = 1.0f
        for(int k = 0; k < 4; ++k)
            invMat *= pivotInv;

        //Row reduction time!
        //except the pivot row
        //Done by subtracting the pivot row from the others
        for(int j = 0; j < 4; ++j)
        {
            if(j != icol)
            {
                float dum = invMat[j][icol];
                invMat[j][icol] = 0.0f
                for(int k = 0; k < 4; ++k)
                    invMat[j][k] -= invMat[icol][k]*dum;
            }
        }
    }
    //the reduction is done now
    //all that is left to do is unscramble
    for(int j = 3; j >= 0; j--)
    {
        if(indxr[j] != indxc[j])
        {
            for(int k = 0; k < 4; ++k)
            {
                std::swap(invMat[k][indxr[j]], invMat[k][indxc[j]]);
            }
        }
    }
    return Matrix4(invMat);
}
}
