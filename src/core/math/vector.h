#ifndef PATH_VECTOR_C
#define PATH_VECTOR_C

#include "../common_core.h"
#include "normal.h"

class Vector
{
    public:
        Vector();
        Vector(const Vector& r);
        Vector(const float& a, const float& b, const float& c);

        explicit Vector(const Normal &n);

        Vector operator*(const float& s) const;
        Vector& operator*=(const float& s);
        Vector operator/(const float& s) const;
        Vector& operator/=(const float& s);

        Vector operator+(const Vector& r) const;
        Vector& operator+=(const Vector& r);
        Vector operator-(const Vector& r) const;
        Vector& operator-=(const Vector& r);

        void normalize();

        Vector operator-() const;

        float x,y,z;
};
#endif
