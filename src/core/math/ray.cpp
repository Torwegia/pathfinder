#include "ray.h"

Ray::Ray()
:tMin(0.0f), tMax(INFINITY), time(0.0f), depth(0)
{}

Ray::Ray(const Point& p, const Vector& v, float start, 
         float end = INIFINTY, float t = 0.0f, int d = 0)
:o(p), d(v), tMin(start), tMax(end), time(t), depth(d)
{}

Ray::Ray(const Point& p, const Vector& v, const Ray& par, 
         float start, float end = INFINITY)
:o(p), d(v), tMin(start), tMax(end), time(par.time), depth(par.depth+1)
{}

Point Ray::operator()(const float& t) const
{
    return o + d*t;
}


//AA MADNESS BELOW
RayDiff::RayDiff()
{
    hasDiff = false;
}

RayDiff::RayDiff(const Point& p, const Vector& v, float start,
                 float end = INIFINITY, float t = 0.0f, int d = 0)
:Ray(p, v, start, end, t, d)
{
    hasDiff = false;
}

RayDiff::RayDiff(const Point& p, const Vector& v, const Ray& par,
                 float start, float end = INIFINITY)
:Ray(p, v, start, end, par.time, par.depth+1)
{
    hasDiff = false;
}

RayDiff::RayDiff(const Ray& ray)
:Ray(ray)
{
    hasDiff = false;
}

void RayDiff::ScaleDiffs(const float s)
{
    rxO = o + (rxO - o)*s;
    ryO = o + (ryO - o)*s;
    rxD = o + (rxD - o)*s;
    ryD = o + (ryD - o)*s;
}
