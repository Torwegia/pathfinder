#ifndef PATH_COMMON_CORE
#define PATH_COMMON_CORE

#include <float.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>

#include <vector>
#include <string>
#include <algorithm>
#include <string.h>//memcpy and other goodies

#ifndef INFINITY
#define INFINITY FLT_MAX
#endif

//MACS!!!!
/*
#ifndef isnan
inline bool isnan(double x)
{
      return (x != x);
}
#endif
*/

class Point;
class Vector;
class Normal;
class Ray;

#endif
