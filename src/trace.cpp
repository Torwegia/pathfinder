#include "scene/scene.h"
#include "camera/camera.h"
#include "camera/image.h"
#include "core/core_geometry.h"
#include "core/color.h"
#include "shapes/shapes.h"
#include "core/random.h"

#include <iostream>


Vector HemiSphericalNorm(const Vector& n)
{
    float r_ang = 2*M_PI*Random::UniformFloat(0.0, 1.0);
    float r_dist = Random::UniformFloat(0.0, 1.0);
    float r_rad = sqrt(r_dist);

    Vector u = n;
    Vector v, w;
    localCoord(u, v, w);


    return (v*cos(r_ang)*r_rad + w*sin(r_ang)*r_rad + u*sqrt(1-r_dist)).norm();
}

Color Trace(Ray r, const Scene& s, int depth)
{
    if(depth > 10)
        return Color(0.0, 0.0, 0.0);

    float result = 100000.0;
    Primitive* hit = NULL;

    for(size_t i = 0; i < s.objects.size(); ++i)
    {
        std::pair<bool, float> test = s.objects[i]->Intersect(r);
        if(test.first)
        {
            if( 0.0 < test.second && test.second < result)
            {
                hit = s.objects[i];
                result = test.second;
            }
        }
    }
    //did the ray strike something in the scene?
    if(hit == NULL)
        return Color(0.0, 0.0, 0.0);
    //return hit->diffuse;

    //it did
    Point loc = r(result);
    Vector norm = hit->Normal(loc);

    Vector dir = HemiSphericalNorm(norm);

    return hit->emittance + (Trace( Ray(loc + 0.0001 * dir, dir, 0.0), s, depth+1) * hit->diffuse);
}

int main(int argc, char **argv)
{
    if(argc < 3)
    {
        std::cout << "gimme number of samples and where to stash it" << std::endl;
        return -1;
    }
    int maxSamp = atoi(argv[1]);
    const char* fileName = argv[2];


    Scene scene( Camera( Point(0.0, 2.0, 0.0), Point(0.0, 0.0, 10.0), Film(2.6, 2.0, 1024, 768)));

    Image img(scene.cam.GetXRes(), scene.cam.GetYRes());

    for(int run = 0.0; run < maxSamp; ++run)
    {
        std::cout << "PASS #" << run+1;
        for(int j = 0; j < scene.cam.GetYRes(); ++j)
        {
            for(int i = 0; i < scene.cam.GetXRes(); ++i)
            {
                img.Set(i,j, img.Get(i,j) + Trace(scene.cam.CastRay(i,j), scene, 0));
            }
        }

        img.Save(run+1, fileName);
        std::cout << "   COMPLETE!" << std::endl;
    }
    return 0;
}
