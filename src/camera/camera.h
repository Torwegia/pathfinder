#ifndef PATH_CAMERA_H
#define PATH_CAMERA_H

#include "film.h"
#include "../core/core_geometry.h"

class Camera
{
    public:
        Camera(Point p, Point f, Film filmPlane);
        Camera(const Camera& c);

        float GetXRes();
        float GetYRes();

        Ray CastRay(int x, int y);
    private:
        Point pos;
        Vector dir;
        Film fp;
};

#endif
