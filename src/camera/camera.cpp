#include "camera.h"
#include "../core/random.h"

Camera::Camera(Point p, Point f, Film filmPlane)
:pos(p), fp(filmPlane)
{
    dir = pos - f;
    dir.normalize();
}

Camera::Camera(const Camera& c)
:pos(c.pos), dir(c.dir), fp(c.fp)
{}

Ray Camera::CastRay(int x, int y)
{
    float jitx = Random::NormalFloat(0.5, 0.2);
    float jity = Random::NormalFloat(0.5, 0.2);
    return Ray( pos, 
                Vector( float(x+jitx) / fp.resX * fp.width - fp.width /2.0,
                        float(y+jity) / fp.resY * fp.height - fp.height / 2.0,
                        1).norm(),
                0.0);
}

float Camera::GetXRes()
{
    return fp.resX;
}

float Camera::GetYRes()
{
    return fp.resY;
}
