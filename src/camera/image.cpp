#include "image.h"
#include <fstream>

Image::Image()
:width(800), height(600), pixBuf(new Color[800*600])
{}

Image::Image(int x, int y)
:width(x), height(y), pixBuf(new Color[x*y])
{}

Image::~Image()
{
    delete pixBuf;
}

Color Image::Get(int x, int y)
{
    return pixBuf[y * width + x];
}

void Image::Set(int x, int y, Color c)
{
    pixBuf[y*width+x] = c;
}

void Image::Add(int x, int y, Color c)
{
    pixBuf[y*width+x] += c;
}


void Image::Save(float samps, const char* filename)
{
    std::ofstream out;
    out.open(filename);

    out << "P3\n" << width << " " << height << "\n255" << std::endl;

    for(int j = 0; j < height; ++j)
        for(int i = 0; i < width; ++i)
        {
            Color pix = pixBuf[j*width +i];
            pix /= samps;
            pix.Clamp();
            out << int(255 * pix.r) << " " << int(255 * pix.g);
            out << " " << int(255 * pix.b) << "\n";
        }

    out.close();
};
