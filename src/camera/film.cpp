#include "film.h"

Film::Film()
:width(4), height(3), resX(800), resY(600)
{}

Film::Film(float w, float h, float x, float y)
:width(w), height(h), resX(x), resY(y)
{}

Film::Film(const Film& f)
:width(f.width), height(f.height), resX(f.resX), resY(f.resY)
{}
