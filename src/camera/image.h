#ifndef PATH_IMAGE_H
#define PATH_IMAGE_H

#include "../core/color.h"
#include <string>

class Image
{
    public:
        Image();
        Image(int x, int y);
        ~Image();

        Color Get(int x, int y);
        void Set(int x, int y, Color);
        void Add(int x, int y, Color);

        void Save(float samps, const char* filename);

        int width, height;
        Color *pixBuf;
};


#endif
