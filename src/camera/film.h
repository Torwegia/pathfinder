#ifndef PATH_FILM_H
#define PATH_FILM_H

class Film
{
    public:
        Film();
        Film(float w, float h, float x, float y);
        Film(const Film&);

        float width, height;
        float resX, resY;
};

#endif
