#ifndef PATH_SPHERE_H
#define PATH_SPHERE_H

#include "primitive.h"
#include "../core/core_geometry.h"

class Sphere : public Primitive
{
    public:
        Sphere(Point p, float r, Color e, Color d);
        Sphere(const Sphere& s);

        std::pair<bool, float> Intersect(const Ray& r);
        Vector Normal(const Point& p);

        Point pos;
        float radius;
};

#endif
