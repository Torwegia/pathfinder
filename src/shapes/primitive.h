#ifndef PATH_PRIM_H
#define PATH_PRIM_H

#include <utility>
#include "../core/core_geometry.h"
#include "../core/color.h"

class Primitive
{
    public:
        Primitive();
        Primitive(Color e, Color d);

        virtual std::pair<bool, float> Intersect(const Ray& r) = 0;
        virtual Vector Normal(const Point& p) = 0;

        Color emittance;
        Color diffuse;
};

#endif
