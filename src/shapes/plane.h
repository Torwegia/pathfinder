#ifndef PATH_PLANE_H
#define PATH_PLANE_H

#include "primitive.h"
#include "../core/common_core.h"
#include "../core/core_geometry.h"

class Plane : public Primitive
{
    public:
        Plane(Point p, Vector n, Color e, Color d);
        Plane(const Plane& p);

        std::pair<bool, float> Intersect(const Ray& r);
        Vector Normal(const Point& p);

        Point pos;
        Vector norm;
};

#endif
