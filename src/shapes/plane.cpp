#include "plane.h"

Plane::Plane(Point p, Vector n, Color e, Color d)
:Primitive(e, d), pos(p), norm(n)
{
    norm.normalize();
}

Plane::Plane(const Plane& p)
:Primitive(p.emittance, p.diffuse), pos(p.pos), norm(p.norm)
{
    norm.normalize();
}

std::pair<bool, float> Plane::Intersect(const Ray& r)
{
    float h = dot(norm, r.d);
    if(h != 0)
    {
        float t = dot((pos - r.o), norm)/h;
        return std::pair<bool, float>(true, t);
    }
    return std::pair<bool, float>(false, 0.0);
}

Vector Plane::Normal(const Point& p)
{
    return norm;
}
