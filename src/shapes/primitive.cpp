#include "primitive.h"

Primitive::Primitive()
:emittance(0.0, 0.0, 0.0), diffuse(0.0, 0.0, 0.0)
{}

Primitive::Primitive(Color e, Color d)
:emittance(e), diffuse(d)
{}
