#include "sphere.h"

Sphere::Sphere(Point p, float r, Color e, Color d)
:Primitive(e, d), pos(p), radius(r)
{}

Sphere::Sphere(const Sphere& s)
:Primitive(s.emittance, s.diffuse), pos(s.pos), radius(s.radius)
{}

std::pair<bool, float> Sphere::Intersect(const Ray& r)
{
    Vector dist = (pos - r.o);
    float b = dot(dist, r.d);
    float c = dot(dist, dist) - radius * radius;
    float d = b*b-c;

    if( d > 0.0 )
    {
        d = sqrt(d);
        if(b-d > 0.0)
            return std::pair<bool, float>(true, (b - d));
        else if(b+d > 0.0)
            return std::pair<bool, float>(true, (b + d));
        else
            return std::pair<bool, float>(false, 0.0);
    }
    else
        return std::pair<bool, float>(false, 0.0);
}

Vector Sphere::Normal(const Point& p)
{
    return (p-pos).norm();
}
