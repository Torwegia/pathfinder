#include "scene.h"

Scene::Scene()
:cam(Point(0.0,-1.0,0.0), Point(0.0, 0.0, 0.0), Film())
{
    //put objects in the vector
}

Scene::Scene(Camera c)
:cam(c)
{
    //make a test
    objects.push_back(new Plane(Point(0.0,10.0,0.0), Vector(0.0,-1.0,0.0), Color(0.0,0.0,0.0), Color( 0.75, 0.75, 0.75 )));
    objects.push_back(new Plane(Point(0.0,-10.0,0.0), Vector(0.0,1.0,0.0), Color(0.0,0.0,0.0), Color( 0.75, 0.75, 0.75 )));
    objects.push_back(new Plane(Point(0.0,0.0,20.0), Vector(0.0,0.0,-1.0), Color(0.0,0.0,0.0), Color( 0.75, 0.75, 0.75 )));

    objects.push_back(new Plane(Point(-10.0,0.0,0.0), Vector(1.0,0.0,0.0), Color(0.0,0.0,0.0), Color( 0.75, 0.25, 0.25 )));
    objects.push_back(new Plane(Point(10.0,0.0,0.0), Vector(-1.0,0.0,0.0), Color(0.0,0.0,0.0), Color( 0.25, 0.25, 0.75 )));

    //balls
    //objects.push_back(new Sphere(Point(-3.5, 13.5, 7.0), 3.0, Color(7.5, 2.5, 2.5)*0.2, Color(0.75, 0.25, 0.25)));
    //objects.push_back(new Sphere(Point(3.5, 13.5, 7.0), 3.0, Color(2.5, 7.5, 2.5)*0.2, Color(0.25, 0.75, 0.25)));
    //objects.push_back(new Sphere(Point(0.0, 10.0, 7.0), 3.0, Color(2.5, 2.5, 7.5)*0.2, Color(0.25, 0.25, 0.75)));
    objects.push_back(new Sphere(Point(0.0, 8.0, 10.0), 1.0, Color(10, 10, 10), Color(0.75, 0.75, 0.75)));
}

Scene::Scene(std::vector<Primitive*> ol, Camera c)
:objects(ol), cam(c)
{}
