#ifndef PATH_SCENE_H
#define PATH_SCENE_H

#include "../shapes/shapes.h"
#include "../camera/camera.h"
#include <vector>

class Scene
{
    public:
        Scene();
        Scene(Camera c);
        Scene(std::vector<Primitive*> ol, Camera c);

        std::vector<Primitive*> objects;
        Camera cam;
    private:
};

#endif
